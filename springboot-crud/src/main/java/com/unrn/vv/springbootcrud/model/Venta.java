package com.unrn.vv.springbootcrud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "venta")
public class Venta {

    @Id
    @GeneratedValue
    private int id;
    private String date; // date no toma, por eso le puse String, cambiar
    private int total;
    private String state;

    public Venta(String date, int total, String state) {
        this.date = date;
        this.total = total;
        this.state = state;
    }
}