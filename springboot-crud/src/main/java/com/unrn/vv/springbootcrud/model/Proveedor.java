package com.unrn.vv.springbootcrud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "proveedor")
public class Proveedor {

    @Id
    @GeneratedValue
    private int id;
    private String street;
    private String city;
    private String state;
    private int postalCode;
    private String name;

    public Proveedor(String street, String city, String state, int postalCode, String name) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.name = name;
    }
}