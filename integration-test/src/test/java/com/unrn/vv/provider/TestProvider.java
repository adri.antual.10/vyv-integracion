package com.unrn.vv.provider;

//package com.unrn.vv.crud;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.elasticsearch.RestClientBuilderCustomizer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.http.client.ClientHttpRequestFactory;
//import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
//import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.unrn.vv.crud.TestH2Repository1;
//import com.unrn.vv.crud.entity.Product;

import com.unrn.vv.crud.entity.Provider;
//import com.unrn.vv.crud.entity.Sale;

//import ch.qos.logback.core.util.Duration;

//import java.time.LocalDate;
//import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestProvider {

    @LocalServerPort
    private int port;

    private String baseUrl = "http://localhost";

    private static RestTemplate restTemplate;

    @Autowired
    private TestH2Repository1 h2Repository1;

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    public static void init() {

        restTemplate = new RestTemplate();

    }

    /***** REQ2 *****/
    // DAR DE ALTA UN PROVEEDOR

    @BeforeEach
    public void setUp() {
        baseUrl = baseUrl.concat(":").concat(port + "").concat("/providers");
    }

    @Test
    public void testAddProvider() {
        Provider provider = new Provider("Sofia", "396162", "Primera junta");
        Provider response = restTemplate.postForObject(baseUrl, provider, Provider.class);
        assertEquals("Sofia", response.getName());
        assertEquals(1, h2Repository1.findAll().size());

    }

    // MODIFICAR UN PROVEEDOR

    @Test
    @Sql(statements = "INSERT INTO providers (id, name, phone, street) VALUES (2,'Rached','12345', 'alvaro barros')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM providers WHERE id=1", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void testUpdateProvider() {
        Provider provider = new Provider("Rached", "12345", "alvaro barros");
        restTemplate.put(baseUrl + "/update/{id}", provider, 2); // ME TOMA EL UPDATE DE PRODUCTO, INSTANCIAR REST???
        Provider providerFromDB = h2Repository1.findById(2).get();
        assertAll(
                () -> assertNotNull(providerFromDB),
                () -> assertEquals("12345", providerFromDB.getPhone()));
    }

    // ELIMINAR UN PROVEEDOR
    @Test
    @Sql(statements = "INSERT INTO providers (id, name, phone, street) VALUES (3, 'maria', '6789','fuente')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void testDeleteProvider() {
        int recordCount = h2Repository1.findAll().size();
        assertEquals(1, recordCount);
        restTemplate.delete(baseUrl + "/{id}", 3);
        assertEquals(0, h2Repository1.findAll().size());
    }
}