package com.unrn.vv.crud;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.elasticsearch.RestClientBuilderCustomizer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.unrn.vv.crud.entity.Product;

import com.unrn.vv.crud.entity.Provider;
import com.unrn.vv.crud.entity.Sale;

import ch.qos.logback.core.util.Duration;

import java.time.LocalDate;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class TestSale {

  @LocalServerPort
    private int port;

    private String baseUrl = "http://localhost";

    private static RestTemplate restTemplate;

    @Autowired
    private TestH2Repository2 h2Repository2;

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    public static void init() {

        restTemplate = new RestTemplate();

    }

    @BeforeEach
    public void setUp() {
        baseUrl = baseUrl.concat(":").concat(port + "").concat("/sales");
    }

    // REGISTRAR UNA VENTA

    @Test
    public void testRegistrarVenta() {
        Sale sale = new Sale(LocalDate.now(), 10000, "pagado");
        Sale response = restTemplate.postForObject(baseUrl, sale, Sale.class);
        assertEquals("pagado", response.getState());
        assertEquals(1, h2Repository2.findAll().size());
    }


 // ACTUALIZAR ESTADO
 
 @Test
 @Sql(statements = "INSERT INTO sales (id, sale_date, total, state) VALUES (8, '2024-05-18', 2000, 'pendiente')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
 @Sql(statements = "DELETE FROM sales WHERE id=8", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
 public void testUpdateSaleEstado() {
     Sale sale = new Sale(LocalDate.now(), 2000, "pago");
     restTemplate.put(baseUrl + "/update/{id}", sale, 8);
     Sale saleFromDB = h2Repository2.findById(8).get();
     assertAll(
             () -> assertNotNull(saleFromDB),
             () -> assertEquals("pago", saleFromDB.getState()));

 }

// ACTUALIZAR FECHA

@Test
@Sql(statements = "INSERT INTO sales (id, sale_date, total, state) VALUES (8, '2024-05-18', 2000, 'pendiente')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(statements = "DELETE FROM sales WHERE id=8", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public void testUpdateSaleFecha() {    
    Sale sale = new Sale(LocalDate.now(), 2000, "pago");
    restTemplate.put(baseUrl + "/update/{id}", sale, 8);
    Sale saleFromDB = h2Repository2.findById(8).get();
    assertAll(
            () -> assertNotNull(saleFromDB),
            () -> assertEquals(LocalDate.now(), saleFromDB.getSale_date()));

}


}
