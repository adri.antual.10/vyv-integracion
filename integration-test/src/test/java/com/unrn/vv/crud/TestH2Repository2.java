package com.unrn.vv.crud;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unrn.vv.crud.entity.Sale;

public interface TestH2Repository2 extends JpaRepository<Sale, Integer> {
}
