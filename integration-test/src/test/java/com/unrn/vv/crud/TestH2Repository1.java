package com.unrn.vv.crud;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unrn.vv.crud.entity.Provider;

public interface TestH2Repository1 extends JpaRepository<Provider, Integer> {
}
