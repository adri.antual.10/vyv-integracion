DROP TABLE IF EXISTS products;

CREATE TABLE products (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  quantity INT NOT NULL,
  price INT DEFAULT NULL
);


CREATE TABLE sales (
    id INT AUTO_INCREMENT PRIMARY KEY,
    sale_date DATE NOT NULL,
    total DOUBLE NOT NULL,
    state VARCHAR(255) NOT NULL
);


/*this.sale_date = sale_date;
        this.total = total;
        this.state = state;
*/