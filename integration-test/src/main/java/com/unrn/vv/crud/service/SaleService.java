package com.unrn.vv.crud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unrn.vv.crud.entity.Sale;
//import com.unrn.vv.crud.repository.ProductRepository;
import com.unrn.vv.crud.repository.SaleRepository;

import java.util.List;

@Service

public class SaleService {

    @Autowired
    private SaleRepository repository;

    public Sale saveSale(Sale sale) {
        return repository.save(sale);
    }

    public List<Sale> saveProducts(List<Sale> sales) {
        return repository.saveAll(sales);
    }

    public List<Sale> getSales() {
        return repository.findAll();
    }

    public Sale getSaleById(int id) {
        return repository.findById(id).orElse(null);
    }

   // public Product getProductByName(String name) {
     //   return repository.findByName(name);
    //}

    public String deleteSale(int id) {
        repository.deleteById(id);
        return "SALE REMUVED !! " + id;
    }

    public Sale updateSale(int saleId, Sale sale) {
        Sale existingSale = repository.findById(saleId).orElse(null);
        existingSale.setSale_date(sale.getSale_date());
        existingSale.setTotal(sale.getTotal());
        existingSale.setState(sale.getState());
        return repository.save(existingSale);
    }



    /*  this.sale_date = sale_date;
        this.total = total;
        this.state = state;
 */

}
