package com.unrn.vv.crud.controller;


import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.unrn.vv.crud.entity.Sale;
import com.unrn.vv.crud.service.SaleService;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/sales")



public class SaleController {

    @Autowired
    private SaleService service;

    @PostMapping
    public Sale addProduct(@RequestBody Sale sale, HttpServletResponse response) {
        
        sale = service.saveSale(sale);
        response.setStatus(HttpServletResponse.SC_CREATED);
        return sale;
    }


    @GetMapping
    public List<Sale> findAllProducts(HttpServletResponse response) {

        return service.getSales();
    }

    @GetMapping("/{id}")
    public Sale findProductById(@PathVariable int id) {
        return service.getSaleById(id);
    }

    @PutMapping("/update/{id}")
    public Sale updateProduct(@RequestBody Sale sale, @PathVariable int id) {
        return service.updateSale(id, sale);
    } 
 
    @DeleteMapping("/{id}")
    public String deleteProduct(@PathVariable int id) {
        return service.deleteSale(id);
    }



}
