package com.unrn.vv.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

//import com.unrn.vv.crud.entity.Product;
import com.unrn.vv.crud.entity.Sale;


public interface SaleRepository extends JpaRepository<Sale,Integer> {
    
}
