package com.unrn.vv.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sales")
public class Sale {

    @Id
    @GeneratedValue
    private int id;
    private LocalDate sale_date;
    private double total;
    private String state;

    public Sale(LocalDate sale_date, double total, String state) {
        this.sale_date = sale_date;
        this.total = total;
        this.state = state;

    }
}





 